#!/usr/bin/python3

import argparse
import os
import re


def argument_parsing():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "directory",
        help="Search for a string \"str\" or 'str' in all the files of a given directory"
    )
    parser.add_argument(
        "--path", 
        action="store_true",
        help="Will show the path of the parent file of the string"
    )
    parser.add_argument(
        "-a", 
        "--all", 
        action="store_true",
        help="Takes into account hidden files"
    )
    parser.add_argument(
        "--suffix",  
        help='''SYNOPSIS 
    strextract --suffix=(suffix) Examine only files ending with (suffix)'''
    )
    arguments = parser.parse_args()
    working_directory = arguments.directory
    path =  arguments.path
    hidden_files = arguments.all
    suffix = arguments.suffix
    
    return working_directory, path, hidden_files, suffix

def search_files():
    
    search_pattern = re.compile(r"\"(.+?)\"|\'(.+?)\'")
    working_directory, path, hidden_files, suffix = argument_parsing()
    for root_path, _, file_names in os.walk(working_directory):
        for file_name in file_names:
            if not hidden_files and file_name.startswith("."):
                continue
            if suffix and not file_name.endswith(suffix):
                continue
            file_path = os.path.join(root_path, file_name)
            try:
                with open(file_path) as current_file: 
                    for line in current_file:
                        str_match = search_pattern.search(line)
                        final_string = str_match.group(0)
                        if str_match:
                            if path:
                                print (f"file path : {file_path}, matched string : {final_string}")
                            else: 
                                print (f"matched string : {final_string}")

            except:
                pass 
def main():
    
    search_files()
    
main()

if __name__ == '__main__':
    main()



