#!/usr/bin/python3

from bs4 import BeautifulSoup
import requests
import argparse
from urllib.parse import urlparse, urlunparse, urljoin

def program_arguments():

    brklnk_args = argparse.ArgumentParser()
    brklnk_args.add_argument("url",
        help="Will search this url for broken links, it will continue going for n levels of depth")
    brklnk_args.add_argument("-n", type=int, default=1,
        help="Will determine how deep the program will go in searching for borken links, the deafult value is 1")
    args = brklnk_args.parse_args()
    return args.url, args.n

def brklnk(url, depth):
 
    if depth < 0:
        return

    response = requests.get(url)
    if response.status_code >= 400:
        print (url)
    src = response.content
    soup = BeautifulSoup(src, "html5lib")   
    links = soup.find_all("a")

    for link in links:
            full_link = urljoin(url, link.attrs["href"])
            brklnk(full_link, depth-1)


def main():
    url, depth = program_arguments()
    brklnk(url, depth)

#if __name__==__main__:

main()
